#!/bin/bash

# Author:      cx537
# Version:     0.0.1
# Date:        2022.3.21
# Email:       2921355083@qq.com
# License:     GPL-3.0（GNU通用公共许可证 v3.0  https://www.gnu.org/licenses/gpl-3.0.html）
# Description: Termux 的一个快速搭建开发环境的脚本。

mirror() {
  sed -i 's@^\(deb.*stable main\)$@#\1\ndeb https://mirrors.tuna.tsinghua.edu.cn/termux/termux-packages-24 stable main@' "$PREFIX"/etc/apt/sources.list
  pkg upgrade -y
}

tools() {
  echo "extra-keys = [['CTRL','<','>','HOME','UP','END','KEYBOARD'],['ESC','{','}','LEFT','DOWN','RIGHT','BACKSLASH'],['TAB','F2','F3','F4','F6','F7','F5']]" >>"$HOME"/.termux/termux.properties
  pkg install python ctags ripgrep neovim nodejs clang git openssh -y
  npm config set registry https://registry.npm.taobao.org
  pip config set global.index-url https://pypi.douban.com/simple/
}

improve_zsh() {
  pkg install zsh -y
  chsh -s zsh

  printf "选择 Ohmyzsh 框架的安装渠道：
  1. 官方渠道（需要梯子）
  2. 备用渠道（从 Github 上备份来的）
  请选择（默认选择备用渠道）："
  read o_select
  case "$o_select" in
  1)
    sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
    case "$?" in
    0)
      echo "安装成功，继续下一步。"
      ;;
    *)
      echo "安装失败，请检查网络后重试。"
      exit
      ;;
    esac
    ;;
  *)
    git clone https://gitee.com/cx537/ohmyzsh.git "$HOME"/.oh-my-zsh
    ;;
  esac

  printf "是否要安装 Powerlevel10k 主题[y(es) or n(o)]
  请选择（默认安装）："
  read p_select
  case "$p_select" in
  n | no | No)
    echo "跳过安装主题操作"
    zshrc_sign="np10k"
    ;;
  *)
    git clone --depth=1 https://gitee.com/romkatv/powerlevel10k.git "$HOME"/.oh-my-zsh/custom/plugins/powerlevel10k
    curl -fsSL -o "$HOME"/.termux/font.ttf https://tool537.oss-cn-shenzhen.aliyuncs.com/font/MesloLGS%20NF%20Regular.ttf
    zshrc_sign="p10k"
    ;;
  esac

  git clone https://gitee.com/cx537/zsh-autosuggestions.git "$HOME"/.oh-my-zsh/custom/plugins/zsh-autosuggestions
  git clone https://gitee.com/cx537/zsh-syntax-highlighting.git "$HOME"/.oh-my-zsh/custom/plugins/zsh-syntax-highlighting

  curl -fsSL -o "$HOME"/.zshrc https://tool537.oss-cn-shenzhen.aliyuncs.com/termux/zshrc

  case "$zshrc_sign" in
  p10k)
    echo -e '\nsource "${ZSH_CUSTOM:-~/.oh-my-zsh/custom}"/plugins/powerlevel10k/powerlevel10k.zsh-theme' >>"$HOME"/.zshrc
    echo 'source "${ZSH_CUSTOM:-~/.oh-my-zsh/custom}"/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh' >>"$HOME"/.zshrc
    echo 'source "${ZSH_CUSTOM:-~/.oh-my-zsh/custom}"/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh' >>"$HOME"/.zshrc
    ;;
  np10k)
    echo 'source "${ZSH_CUSTOM:-~/.oh-my-zsh/custom}"/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh' >>"$HOME"/.zshrc
    echo 'source "${ZSH_CUSTOM:-~/.oh-my-zsh/custom}"/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh' >>"$HOME"/.zshrc
    ;;
  esac

  echo "" >"$PREFIX"/etc/motd
}

nvim_config() {
  pip install pynvim
  sh -c 'curl -fLso "$HOME"/.config/nvim/init.vim --create-dirs https://tool537.oss-cn-shenzhen.aliyuncs.com/vim/init.vim'
  sh -c 'curl -fLso "${XDG_DATA_HOME:-$HOME/.local/share}"/nvim/site/autoload/plug.vim --create-dirs https://tool537.oss-cn-shenzhen.aliyuncs.com/vim/plug.vim'
  curl -fsLo "$HOME"/temp.zip https://tool537.oss-cn-shenzhen.aliyuncs.com/vim/nvimPlugin.zip
  unzip "$HOME"/temp.zip
  rm "$HOME"/temp.zip
}

main() {
  printf "提供以下操作：
  1. 执行所有操作   2. 换（清华）源
  3. 工具下载      4. 优化终端
  5. NeoVim 配置
  请选择：
  "
  read m_select

  case "$m_select" in
  1)
    mirror && tools && improve_zsh && nvim_config
    ;;
  2)
    mirror
    ;;
  3)
    tools
    ;;
  4)
    improve_zsh
    ;;
  5)
    nvim_config
    ;;
  *)
    echo "没有该选项！"
    exit
    ;;
  esac

  rm -rf "$HOME"/termux.sh
  echo "配置完成，请退出后重新启动！"
}

main
