# TConfig

#### 介绍

tconfig.sh 是一个本着**简洁**，**清晰**，**易用**等原则的 Termux 快速配置脚本。

#### 使用说明

打开 Termux，执行以下命令即可。

```shell
sh -c "$(curl -fsSL https://tool537.oss-cn-shenzhen.aliyuncs.com/termux/tconfig.sh)"
```

#### 参与贡献

遇到 Bug 或有什么想法可以提 Issues。同时也希望有能力的人可以帮助改善源码，谢谢！

#### END

[本人博客](https://cx537.cn)有一些关于 Termux 的使用记录，有兴趣的可以前往看看。